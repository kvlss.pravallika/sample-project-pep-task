package com.epam;

import com.epam.datamodel.Difficulty;
import com.epam.datamodel.Option;
import com.epam.datamodel.Question;
import com.epam.datamodel.Users;
import com.epam.service.QuestionService;
import com.epam.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Configuration
@ComponentScan
public class App {
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(App.class);
    private static final QuestionService questionService = context.getBean(QuestionService.class);
    private static final UserService userService = context.getBean(UserService.class);
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        userMenu(bufferedReader);
        int choice;
        do {
            logger.info("\n 1. Create 2. Update 3. Delete 4. Read 5. Display All Questions 6. Exit");
            choice = Integer.parseInt(bufferedReader.readLine());
            switch (choice) {
                case 1:
                    createQuestion(bufferedReader);
                    break;
                case 2:
                    updateQuestion(bufferedReader);
                    break;
                case 3:
                    deleteQuestion(bufferedReader);
                    break;
                case 4:
                    getParticularQuestion(bufferedReader);
                    break;
                case 5:
                    displayAllQuestion();
                    break;
                case 6:
                    break;
                default:
                    logger.info("Wrong input. Enter Again");
            }
        } while (choice != 6);
    }

    private static void userMenu(BufferedReader bufferedReader) throws IOException {
        logger.info("\n 1. Login \n 2. SignUp \n");
        int choice = Integer.parseInt(bufferedReader.readLine());
        switch (choice) {
            case 1:
                loginUser();
                break;
            case 2:
                inputUser();
                break;
            default:
                break;
        }
    }

    private static void createQuestion(BufferedReader bufferedReader) throws IOException {
        Question question = getQuestion(bufferedReader);
        displayMessage("Question is successfully created", "Question not created", questionService.insertQuestion(question));
    }

    private static Question getQuestion(BufferedReader bufferedReader) throws IOException {
        logger.info("******* Enter Question ********");
        logger.info("Enter Question Details: ");
        String questionDetails = bufferedReader.readLine();
        logger.info("Enter Question marks: ");
        int score = Integer.parseInt(bufferedReader.readLine());
        logger.info("Choose the difficulty\n1. Hard\n2. Easy\n3. Medium");
        int choice = Integer.parseInt(bufferedReader.readLine());
        Difficulty questionCategory = Difficulty.EASY;
        if (choice == 1)
            questionCategory = Difficulty.HARD;
        else if (choice == 2)
            questionCategory = Difficulty.EASY;
        else if (choice == 3)
            questionCategory = Difficulty.MEDIUM;
        List<Option> options = createOptions(bufferedReader);
        Question question = new Question(questionDetails, score, questionCategory);
        question.setOptions(options);
        return question;
    }

    private static void updateQuestion(BufferedReader bufferedReader) throws IOException {
        displayAllQuestion();
        logger.info("Enter the Question Id to update");
        Long questionNumber = Long.parseLong(bufferedReader.readLine());
        Question question = getQuestion(bufferedReader);
        question.setQuestionId(questionNumber);
        displayMessage("Update successful", "Update unsuccessful", questionService.updateQuestion(question, questionNumber));
    }

    private static void getParticularQuestion(BufferedReader bufferedReader) throws IOException {
        logger.info("Enter question number to be read");
        Long questionId = Long.parseLong(bufferedReader.readLine());
        Optional<Question> questionOptional = questionService.getQuestion(questionId);
        if (questionOptional.isPresent()) {
            Question question = questionOptional.get();
            logger.info("Question {}", question.getQuestionId());
            logger.info("Question Description: {}", question.getQuestionDescription());
            logger.info("Question Option: ");
            question.getOptions().forEach(option -> logger.info(option.getOptionDescription()));
        } else {
            logger.info("Invalid Question number");
        }
    }

    private static void deleteQuestion(BufferedReader bufferedReader) throws IOException {
        displayAllQuestion();
        logger.info("Enter question Id to be deleted:");
        Long questionNumber = Long.parseLong(bufferedReader.readLine());
        displayMessage("Delete successful", "Invalid question number", questionService.deleteQuestion(questionNumber));
    }

    private static void displayAllQuestion() {
        List<Question> questions = questionService.getAllQuestion();
        questions.forEach(question -> logger.info("Question Number:- {} \t {}", question.getQuestionId(), question.getQuestionDescription()));
    }

    private static List<Option> createOptions(BufferedReader bufferedReader) throws IOException {
        logger.info("Enter the four options:-");
        List<Option> options = new ArrayList<>();
        int i = 0;
        while (i++ < 4) {
            logger.info("\n Enter option Details: ");
            String optionDescription = bufferedReader.readLine();
            logger.info("\n Enter whether it is a correct answer(true|false)");
            boolean isCorrect = Boolean.parseBoolean(bufferedReader.readLine());
            options.add(new Option(optionDescription, isCorrect));
        }
        return options;
    }

    private static void loginUser() throws IOException {
        logger.info("******** Enter user Detail ********");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        logger.info("\n Enter password ");
        String password = reader.readLine();
        logger.info("\n Enter email Id ");
        String emailId = reader.readLine();
        if (!emailId.isEmpty() && !password.isEmpty()) {
            displayMessage("login successful", "login unsuccessful", userService.login(emailId, password));
        }
    }

    private static void inputUser() throws IOException {
        logger.info("******** Enter user Detail ********");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        logger.info(" Enter name ");
        String name = reader.readLine();
        logger.info("\n Enter password ");
        String password = reader.readLine();
        logger.info("\n Enter email Id ");
        String emailId = reader.readLine();
        if (validate(name, emailId, password)) {
            displayMessage("SignUp successful", "SignUp unsuccessful",
                    userService.signUp(new Users(name, emailId, password, false)));
        }
    }

    private static boolean validate(String name, String emailId, String password) {
        return !name.isEmpty() && !emailId.isEmpty() && !password.isEmpty();
    }

    private static void displayMessage(String successMessage, String failureMessage, boolean isSuccess) {
        logger.info(isSuccess ? successMessage : failureMessage);

    }
}
