package com.epam.datamodel;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}
