package com.epam.datamodel;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long quizId;

    @Column(nullable = false)
    private int duration;

    @Column(nullable = false)
    private String topic;

    @Column(nullable = false)
    private int noOfQuestions;

    @Column(nullable = false)
    private int totalScore;

    @OneToMany(targetEntity = Question.class, mappedBy = "quiz", cascade = CascadeType.ALL)
    private List<Question> questions;

    public Quiz() {

    }

    public Quiz(int duration, String topic, int noOfQuestions, int totalScore) {
        this.duration = duration;
        this.topic = topic;
        this.noOfQuestions = noOfQuestions;
        this.totalScore = totalScore;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(int noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
        for (Question question : questions)
            question.setQuiz(this);
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "quizId=" + quizId +
                ", duration=" + duration +
                ", topic='" + topic + '\'' +
                ", noOfQuestions=" + noOfQuestions +
                ", totalScore=" + totalScore +
                ", questions=" + questions +
                '}';
    }
}
