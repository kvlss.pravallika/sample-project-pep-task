package com.epam.repositoryimpl;

import com.epam.datamodel.Quiz;
import com.epam.repository.QuizRepository;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class QuizRepositoryImpl implements QuizRepository {
    private static final EntityManager entityManager = Persistence
            .createEntityManagerFactory("QuizJPA")
            .createEntityManager();

    @Override
    public boolean createQuiz(Quiz quiz) {
        boolean success = false;
        if (Objects.nonNull(quiz)) {
            entityManager.getTransaction().begin();
            entityManager.persist(quiz);
            entityManager.getTransaction().commit();
            success = true;
        }
        return success;
    }

    @Override
    public boolean deleteByQuizId(Long quizId) {
        boolean success = false;
        entityManager.getTransaction().begin();
        Quiz quiz = entityManager.find(Quiz.class, quizId);
        if (quiz != null) {
            entityManager.remove(quiz);
            success = true;
        }
        entityManager.getTransaction().commit();
        return success;
    }

    @Override
    public List<Quiz> getAllQuiz() {
        return entityManager.createQuery("SELECT q FROM Quiz q", Quiz.class).getResultList();
    }

    @Override
    public Optional<Quiz> getQuizById(Long quizId) {
        Quiz quiz = entityManager.find(Quiz.class, quizId);
        return quiz != null ? Optional.of(quiz) : Optional.empty();
    }
}
