package com.epam.service;

import com.epam.datamodel.Question;
import com.epam.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class QuestionService {
    
    @Autowired
    QuestionRepository questionsRepository;

    public List<Question> getAllQuestion() {
        return questionsRepository.getAllQuestion();
    }

    public boolean insertQuestion(Question question) {
        return questionsRepository.insertQuestion(question);
    }

    public boolean deleteQuestion(Long questionId) {
        return questionsRepository.deleteQuestion(questionId);
    }

    public boolean updateQuestion(Question question, Long questionId) {
        return questionsRepository.updateQuestion(question, questionId);
    }

    public Optional<Question> getQuestion(Long questionId) {
        return questionsRepository.getQuestion(questionId);
    }
}
