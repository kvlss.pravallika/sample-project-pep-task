package com.epam.service;

import com.epam.datamodel.Quiz;
import com.epam.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class QuizService {

    @Autowired
    QuizRepository quizRepository;

    public boolean createQuiz(Quiz quiz) {
        return quizRepository.createQuiz(quiz);
    }

    public boolean deleteByQuizId(Long quizId) {
        return quizRepository.deleteByQuizId(quizId);
    }

    public List<Quiz> getAllQuiz() {
        return quizRepository.getAllQuiz();
    }

    public Optional<Quiz> getQuizById(Long quizId) {
        return quizRepository.getQuizById(quizId);
    }
}
