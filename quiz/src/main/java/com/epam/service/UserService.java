package com.epam.service;

import com.epam.datamodel.Users;
import com.epam.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService {

    @Autowired
    UserRepository userRepository;

    public boolean signUp(Users user) {
        return userRepository.signUp(user);
    }

    public boolean login(String email, String password) {
        return userRepository.login(email, password);
    }
}
