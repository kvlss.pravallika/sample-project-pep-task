package com.epam.repositoryimpl;

import com.epam.datamodel.Quiz;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class QuizRepositoryImplTest {

    private QuizRepositoryImpl quizRepositoryImpl;
    private List<Quiz> quizzes;

    @BeforeEach
    void setUp() {
        quizRepositoryImpl = new QuizRepositoryImpl();
        quizzes = Arrays.asList(
               new Quiz( 30, "Topic1", 10, 30),
               new Quiz(30,"Topic2", 10, 30)
        );
        quizzes.forEach(quiz -> quizRepositoryImpl.createQuiz(quiz));
    }

    @Test
    public void testCreateQuiz() {
        Quiz quiz = new Quiz(30,"Topic3", 10, 30);
        boolean result = quizRepositoryImpl.createQuiz(quiz);
        assertTrue(result);
    }

    @Test
    public void testCreateQuizWhenQuizIsNull() {
        boolean result = quizRepositoryImpl.createQuiz(null);
        assertFalse(result);
    }

    @Test
    public void testDeleteByQuizId() {
        boolean result = quizRepositoryImpl.deleteByQuizId(1L);
        assertTrue(result);
    }

    @Test
    public void testDeleteWhenQuizIdIsNotPresent() {
        boolean result = quizRepositoryImpl.deleteByQuizId(7L);
        assertFalse(result);
    }

    @Test
    public void testGetAllQuiz() {
        List<Quiz> quizList = quizRepositoryImpl.getAllQuiz();

        assertEquals(2, quizList.size());
        for(int i =0; i<2 ; i++) {
            assertEquals(quizzes.get(i).getQuizId(), quizList.get(i).getQuizId());
            assertEquals(quizzes.get(i).getDuration(), quizList.get(i).getDuration());
            assertEquals(quizzes.get(i).getNoOfQuestions(), quizList.get(i).getNoOfQuestions());
            assertEquals(quizzes.get(i).getTopic(), quizList.get(i).getTopic());
            assertEquals(quizzes.get(i).getTotalScore(), quizList.get(i).getTotalScore());
        }
    }

    @Test
    public void testGetQuizWhenQuizIdIsPresent() {
        Optional<Quiz> resultQuizOptional = quizRepositoryImpl.getQuizById(2L);

        assertTrue(resultQuizOptional.isPresent());
        assertEquals(quizzes.get(1).getQuizId(), resultQuizOptional.get().getQuizId());
        assertEquals(quizzes.get(1).getDuration(), resultQuizOptional.get().getDuration());
        assertEquals(quizzes.get(1).getNoOfQuestions(), resultQuizOptional.get().getNoOfQuestions());
        assertEquals(quizzes.get(1).getTopic(), resultQuizOptional.get().getTopic());
        assertEquals(quizzes.get(1).getTotalScore(), resultQuizOptional.get().getTotalScore());
    }

    @Test
    public void testGetQuizWhenQuizIdIsNotPresent() {
        Optional<Quiz> resultQuizOptional = quizRepositoryImpl.getQuizById(7L);
        assertFalse(resultQuizOptional.isPresent());
        assertEquals(Optional.empty(), resultQuizOptional);
    }

}