package com.epam.repositoryimpl;

import com.epam.datamodel.Users;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryImplTest {

    private UserRepositoryImpl userRepositoryImpl;
    private List<Users> usersList;

    @BeforeEach
    void setUp() {
        userRepositoryImpl = new UserRepositoryImpl();
        usersList = Arrays.asList(
                new Users("admin", "admin001@gmail.com", "admin", true ),
                new Users("xyz", "xyz02@gmail.com", "user", false)
        );
        usersList.forEach(user -> userRepositoryImpl.signUp(user));
    }

    @Test
    public void testSignUpWhenUserIsNotNull() {
        // Given
        Users user = new Users("abcd", "abcd@gmail.com", "abcd", false);

        // When
        boolean result = userRepositoryImpl.signUp(user);

        // Then
        assertTrue(result);
    }

    @Test
    public void testSignUpWhenUserIsNull() {
        boolean result = userRepositoryImpl.signUp(null);
        assertFalse(result);
    }

    @Test
    public void testLoginWhenEmailIdIsPresentAndPasswordIsValid() {
        boolean result = userRepositoryImpl.login("xyz02@gmail.com", "user");
        assertTrue(result);
    }

    @Test
    public void testLoginWhenEmailIdIsNotPresentAndPasswordIsValid() {
        boolean result = userRepositoryImpl.login("qwerty", "user");
        assertFalse(result);
    }

    @Test
    public void testLoginWhenEmailIdAndPasswordAreEmpty() {
        boolean result = userRepositoryImpl.login("", "");
        assertFalse(result);
    }

    @Test
    public void testLoginWhenEmailIdIsEmpty() {
        boolean result = userRepositoryImpl.login("", "user");
        assertFalse(result);
    }

    @Test
    public void testLoginWhenEmailIdIsPresentAndPasswordIsInvalid() {
        boolean result = userRepositoryImpl.login("xyz02@gmail.com", "xyz");
        assertFalse(result);
    }

    @Test
    public void testLoginWithEmailIdIsPresentAndPasswordIsEmpty() {
        boolean result = userRepositoryImpl.login("xyz02@gmail.com", "");
        assertFalse(result);
    }

}